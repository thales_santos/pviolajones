from detector import Detector
import Image
import ImageDraw
import sys

try:
    detector = Detector('./resources/haarcascade_frontalface_default.xml')
    img = Image.open('./resources/lena.jpg')
    
    #faces = detector.getFaces(img, 1, 1.25, 0.1, 1, False)
    faces = detector.getFaces(img, 2, 1.25, 0.1, 3, False)
    
    for face in faces:
        draw = ImageDraw.Draw(img)
        x1 = face['x']
        y1 = face['y']
        x2 = face['x'] + face['width']
        y2 = face['y'] + face['height']
        draw.rectangle([x1, y1, x2, y2])
        img.save('./resources/lena_face.jpg')
except Exception as ex:
    print ex 
except:
    print "Unexpected error:", sys.exc_info()[0]
    raise