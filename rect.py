class Rect:
    
    def __init__(self, rect):
        split_rect = rect.split(' ')
        self.x1 = int(split_rect[0])
        self.x2 = int(split_rect[1])
        self.y1 = int(split_rect[2])
        self.y2 = int(split_rect[3])
        self.weight = float(split_rect[4])

if __name__ == "__main__":
    rectStr = "1 2 3 4 5.1"
    rect = Rect(rectStr)
    print rectStr
    print rect.x1
    print rect.x2
    print rect.y1
    print rect.y2
    print rect.weight