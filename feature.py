from math import sqrt


class Feature:

    def __init__(self, threshold, left_val, left_node, has_left_val, right_val, right_node, has_right_val, size):
        self.nb_rects = 0
        self.rects = []
        self.threshold = threshold
        self.left_val = left_val
        self.left_node = left_node
        self.has_left_val = has_left_val
        self.right_val = right_val
        self.right_node = right_node
        self.has_right_val = has_right_val
        # size must be a dictionary containing x and y keys
        self.size = size

    def getLeftOrRight(self, grayImage, squares, i, j, scale):
        w = int(scale * self.size['x'])
        h = int(scale * self.size['y'])
        inv_area = 1.0/(w * h)

        total_x = grayImage[i+w, j+h] + grayImage[i, j] - grayImage[i, j+h] - grayImage[i+w, j]
        total_x2 = squares[i+w, j+h] + squares[i, j] - squares[i, j+h] - squares[i+w, j]

        mean = total_x * inv_area
        vnorm = (total_x2 * inv_area) - (mean * mean)

        vnorm = sqrt(vnorm) if (vnorm > 1) else 1

        rect_sum = 0

        for rect in self.rects:

            rx1 = i + int(scale * rect.x1)
            rx2 = i + int(scale * (rect.x1 + rect.y1))
            ry1 = j + int(scale * rect.x2)
            ry2 = j + int(scale * (rect.x2 + rect.y2))

            rect_sum += int(((grayImage[rx2, ry2] - grayImage[rx1, ry2] - grayImage[rx2, ry1] + grayImage[rx1, ry1]) * rect.weight))

        rect_sum2 = rect_sum * inv_area
        result = 0 if (rect_sum2 < self.threshold * vnorm) else 1

        return result

    def add(self, rect):
        self.rects.append(rect)
        self.nb_rects += 1