LEFT = 0
RIGHT = 1

class Tree:

    def __init__(self):

        self.features = []

    def getVal(self, grayImage, squares, i, j, scale):
        cur_node = self.features[0]

        while True:
            where = cur_node.getLeftOrRight(grayImage, squares, i, j, scale)
            if (where == LEFT):
                if (cur_node.has_left_val):
                    return cur_node.left_val
                else:
                    cur_node = self.features[cur_node.left_node]
            else:
                if (cur_node.has_right_val):
                    return cur_node.right_val
                else:
                    cur_node = self.features[cur_node.right_node]

    def addFeature(self, feature):

        self.features.append(feature)