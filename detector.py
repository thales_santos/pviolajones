import xml.etree.ElementTree as ET
import numpy as np
from stage import Stage
from tree import Tree
from feature import Feature
from rect import Rect
import Image

class Detector:

    def __init__(self, haarcascade_file):

        self.stages = []
        self.size = {}

        haarcascade_xml = ET.parse(haarcascade_file)
        root = haarcascade_xml.getroot()

        size_element = root.find("./haarcascade_frontalface_default/size")
        split_size = size_element.text.split(' ')

        self.size['x'] = int(split_size[0])
        self.size['y'] = int(split_size[1])

        stages_element = root.findall("./haarcascade_frontalface_default/stages/_")

        for stage_element in stages_element:

            threshold = float(stage_element.find("stage_threshold").text)
            trees_element = stage_element.findall("trees/_")

            stage = Stage(threshold)

            for tree_element in trees_element:
                tree = Tree()

                features_element = tree_element.findall("_")
                
                for feature_element in features_element:

                    feature_threshold = float(feature_element.find("threshold").text)
                    left_node = -1
                    left_val = 0
                    has_left_val = False
                    right_node=-1
                    right_val = 0
                    has_right_val =False

                    if (feature_element.find("left_val") is not None):
                        left_val = float(feature_element.find("left_val").text)
                        has_left_val = True
                    else:
                        left_node = int(feature_element.find("left_node").text)
                        has_left_val = False

                    if (feature_element.find("right_val") is not None):
                        right_val = float(feature_element.find("right_val").text)
                        has_right_val = True
                    else:
                        right_node = int(feature_element.find("right_node").text)
                        has_right_val = False

                    feature = Feature(feature_threshold, left_val, left_node, has_left_val, \
                                        right_val, right_node, has_right_val, self.size)

                    rect_elements = feature_element.findall("./feature/rects/_")

                    for rect_element in rect_elements:
                        rect_text = rect_element.text

                        feature.add(Rect(rect_text))

                    tree.addFeature(feature)

                stage.addTree(tree)

            self.stages.append(stage)

    def getIntegralCanny(self, grayImage):
        m, n = grayImage.shape
        canny = np.zeros((m, n))

        for i in range(2, (m - 2), 1):
            for j in range(2, (n - 2), 1):
                sum_result = 0
                sum_result += 2 * grayImage[i-2, j-2]
                sum_result += 4 * grayImage[i-2, j-1]
                sum_result += 5 * grayImage[i-2, j+0]
                sum_result += 4 * grayImage[i-2, j+1]
                sum_result += 2 * grayImage[i-2, j+2]
                sum_result += 4 * grayImage[i-1, j-2]
                sum_result += 9 * grayImage[i-1, j-1]
                sum_result += 12 * grayImage[i-1, j+0]
                sum_result += 9 * grayImage[i-1, j+1]
                sum_result += 4 * grayImage[i-1, j+2]
                sum_result += 5 * grayImage[i+0, j-2]
                sum_result += 12 * grayImage[i+0, j-1]
                sum_result += 15 * grayImage[i+0, j+0]
                sum_result += 12 * grayImage[i+0, j+1]
                sum_result += 5 * grayImage[i+0, j+2]
                sum_result += 4 * grayImage[i+1, j-2]
                sum_result += 9 * grayImage[i+1, j-1]
                sum_result += 12 * grayImage[i+1, j+0]
                sum_result += 9 * grayImage[i+1, j+1]
                sum_result += 4 * grayImage[i+1, j+2]
                sum_result += 2 * grayImage[i+2, j-2]
                sum_result += 4 * grayImage[i+2, j-1]
                sum_result += 5 * grayImage[i+2, j+0]
                sum_result += 4 * grayImage[i+2, j+1]
                sum_result += 2 * grayImage[i+2, j+2]

                canny[i, j] = sum_result / 159

        grad = np.zeros((m, n))
        
        for i in range(m -1):
            for j in range(n -1):

                grad_x = - canny[i-1, j-1] + canny[i+1, j-1] - 2*canny[i-1, j] + 2*canny[i+1, j] - canny[i-1, j+1] + canny[i+1, j+1]
                grad_y = canny[i-1, j-1] + 2*canny[i, j-1] + canny[i+1, j-1] - canny[i-1, j+1] - 2*canny[i, j+1] - canny[i+1, j+1]
                grad[i, j] = abs(grad_x) + abs(grad_y)

        for i in range(m):
            col = 0

            for j in range (n):
                value = grad[i, j]
                canny[i, j] = (canny[i-1, j] if (i >0) else 0) + col + value
                col += value

        return canny

    def equals(self, r1, r2):
        distance = int(r1['width'] * 0.2)

        if (r2['x'] <= r1['x'] + distance and r2['x'] >= r1['x'] - distance and r2['y'] <= r1['y'] + distance and r2['y'] >= r1['y'] - distance and r2['width'] <= int(r1['width'] * 1.2) and int(r2['width'] * 1.2 ) >= r1['width']):
            return True

        if (r1['x'] >= r2['x'] and r1['x'] + r1['width'] <= r2['x'] + r2['width'] and r1['y'] >= r2['y'] and r1['y'] + r1['height'] <= r2['y'] + r2['height']):
            return True

        return False

    def merge(self, rects, min_neighbors):
        result = []

        ret = [None] * len(rects)

        nb_classes = 0

        for i in range(len(rects)):

            found = False

            for j in range(i):

                if (self.equals(rects[j], rects[i])):
                    found = True
                    ret[i] = ret[j]

            if (found is not True):
                ret[i] = nb_classes
                nb_classes += 1

        neighbors = [0] * nb_classes
        rect = [None] * nb_classes

        for i in range(nb_classes):
            rect[i] = {'width':0, 'height':0, 'x':0, 'y':0}

        for i in range(len(rects)):

            neighbors[ret[i]] += 1

            r2 = rects[i]
            r = rect[ret[i]]

            r['x'] += r2['x']
            r['y'] += r2['y']
            r['height'] += r2['height']
            r['width'] += r2['width']

        for i in range(nb_classes):
            n = neighbors[i]

            if (n >= min_neighbors):

                r = {'width':0, 'height':0, 'x':0, 'y':0}
                r2 = rect[i]

                r['x'] = (r2['x']*2 + n) / (2*n)
                r['y'] = (r2['y']*2 + n) / (2*n)
                r['width'] = (r2['width']*2 + n) / (2*n)
                r['height'] = (r2['height']*2 + n) / (2*n)

                result.append(r)

        return result

    def getFaces(self, image, baseScale, scale_inc, increment, min_neighbors, doCannyPruning):

        ret = []

        width, height = image.size

        maxScale = min((width / self.size['x']), (height / self.size['y']))

        img = np.zeros(shape=(width, height))
        grayImage = np.zeros(shape=(width, height))
        squares = np.zeros(shape=(width, height))

        pixels = image.load()

        for i in range(width):

            col = 0
            col2 = 0

            for j in range(height):

                red, green, blue = pixels[i, j]

                value = (30*red + 59*green + 11*blue)/100

                img[i, j] = value
                grayImage[i, j] = (grayImage[i-1, j] if (i > 0) else 0) + col + value
                squares[i, j] = (squares[i-1, j] if (i > 0) else 0) + col2 + value * value
                col += value
                col2 += value * value

        canny = None

        if(doCannyPruning):
            canny = self.getIntegralCanny(img)

        scale = baseScale
        while (scale < maxScale):

            step = int(scale * 24 * increment)
            sz = int(scale * 24)

            #for (int i = 0; i < width - size; i += step) {

            for i in range(0, width - sz, step):

                for j in range(0, height - sz, step):

                    if (doCannyPruning):

                        edges_density = canny[i+sz, j+sz]+canny[i, j]-canny[i, j+sz]-canny[i+sz, j]
                        d = edges_density / sz / sz

                        if(d < 20 or d > 100):
                            continue

                    skipped = True
                    k = 0

                    for stage in self.stages:

                        if not stage.skip(grayImage, squares, i, j, scale):
                            skipped = False
                            break

                        k += 1

                    if (skipped):
                        ret.append({'width':sz, 'height':sz, 'x':i, 'y':j})

            scale *= scale_inc

        return self.merge(ret, min_neighbors)