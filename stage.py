class Stage:

    def __init__(self, threshold):

        self.threshold = threshold
        self.trees = []

    def skip(self, grayImage, squares, i, j, scale):

        sum_result = 0.0
        for tree in self.trees:
            sum_result += tree.getVal(grayImage, squares,i, j, scale)

        return (sum_result > self.threshold)

    def addTree(self, tree):

        self.trees.append(tree)