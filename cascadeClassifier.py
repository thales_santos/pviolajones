'''
Created on 17/07/2014

@author: felipe.franco
'''
import numpy
import cv2


class CascadeClassifier:
    def __init__(self, cascadePath, args):
        self.classifier = cv2.CascadeClassifier('./resources/haarcascade_frontalface_default.xml')
        self.scaleFactor = args['scaleFactor'] if args.has_key('scaleFactor') else 1.2
        self.minNeighbors = args['minNeighbors'] if args.has_key('minNeighbors') else 5
        self.turnToGrayFirst = args['turnToGrayFirst'] if args.has_key('turnToGrayFirst') else True

    def classify(self, image):
        '''Return a list of the bottom left and top right corners of the detected objects as tuples
        [ ( (bottomLeft0,topRight0) ), ( (bottomLeft1,topRight1) ) , ...], where bottomLeft=(x,y)
        '''
        imageToDetect = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY) if self.turnToGrayFirst else image 
        faces = []
        for (x,y,w,h) in self.classifier.detectMultiScale(imageToDetect, self.scaleFactor, self.minNeighbors):
            faces.append([(x,y), (x+w, y+h)])
        return faces


def paintRectangle(image, rectangle):
    cv2.rectangle(image, rectangle[0], rectangle[1], (255,0,0), 2)


def playMoviePaitingDetectedObjects(moviePath, classifier):
    cap = cv2.VideoCapture(moviePath)

    while(cap.isOpened()):
        #read the frame
        ret, frame = cap.read()

        #paint the boxes around the classified objects
        for detectedObjectRectangle in classifier.classify(frame):
            paintRectangle(frame, detectedObjectRectangle)

        cv2.imshow('frame', frame)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    cap.release()
    cv2.destroyAllWindows()


def displayImageWithPaintedObjects(imagePath, classifier):
    image = cv2.imread(imagePath)
    for rectangle in classifier.classify(image):
        paintRectangle(image, rectangle)
    cv2.imshow('frame', image)
    cv2.waitKey()


if __name__ == '__main__':
    frotalFaceClassifier = CascadeClassifier('./resources/haarcascade_frontalface_default.xml', {'scaleFactor':1.2, 'minNeighbors':5})
    moviePath = './resources/testVideo.mp4'
    imagePath = './resources/colorPeople.jpg'

    displayImageWithPaintedObjects(imagePath, frotalFaceClassifier)
    playMoviePaitingDetectedObjects(moviePath, frotalFaceClassifier)


